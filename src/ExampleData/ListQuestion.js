function ListQuestion() {
    return [{
        id: 1,
        question: 'Câu 1: Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
            'Trong các mệnh đề sau, mệnh đề nào đúng?',
        A: 'A: f (−2) < f (3).',
        B: 'B: f (−2) < f (5).',
        C: 'C: f (4) < f (5)',
        D: 'D: f (−1) < f (4)',
        checked: false
    }, {
        id: 2,
        question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
            'Trong các mệnh đề sau, mệnh đề nào đúng?',
        A: 'A: f (−2) < f (3).',
        B: 'B: f (−2) < f (5).',
        C: 'C: f (4) < f (5)',
        D: 'D: f (−1) < f (4)',
        checked: false
    },
        {
            id: 3,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 4,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 5,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 6,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 7,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 8,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 9,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 10,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 11,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 12,
            question: 'Câu 1: Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        }, {
            id: 13,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 14,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 15,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 16,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 17,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 18,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 19,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 20,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 21,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
        {
            id: 22,
            question: 'Cho hàm số y = f (x) xác định trên khoảng (−2; 5) và có đạo hàm f0(x) > 0, ∀x ∈ (−2; 5).\n' +
                'Trong các mệnh đề sau, mệnh đề nào đúng?',
            A: 'A: f (−2) < f (3).',
            B: 'B: f (−2) < f (5).',
            C: 'C: f (4) < f (5)',
            D: 'D: f (−1) < f (4)',
            checked: false
        },
    ]
}

export default ListQuestion