import React, {useState} from "react";
import {Button, Col, Container, Form, Image, ProgressBar, Row} from "react-bootstrap";
import {useHistory} from "react-router-dom";

function FormRecommend() {
    const [process, setProcess] = useState(50)
    const [isNext, setIsNext] = useState(false)
    let history = useHistory();
    function redirect() {
        history.push("/");
    }
    function NextProcess() {
        setProcess(100);
        setIsNext(true)
    }

    const data = new Array('<=5', '5<x<8', 'x>=8')
    const title = ['Điểm toán trung bình của bạn?', "Điểm hoá trung bình của bạn?", "Điểm lý trung bình của bạn?", "Điểm anh văn trung bình của bạn?"]

    function Form1() {
        return (<Row style={{margin: 15, textAlign: "center"}}>
            <div className="mt-5">
                <input style={{width: "100%"}} type="text" placeholder="Họ và tên"/>
            </div>
            <div className="mb-5 justify-content-between mt-5" style={{display: "flex"}}>
                <input style={{width: "60%"}} type="text" placeholder="Nguyện vọng: tốt nghiệp hay đại học"/>
                <input style={{width: "35%"}} type="text" placeholder="Số điểm mong ước "/>
            </div>
        </Row>)
    }

    function Form2() {
        return title.map((v, id) => {
            return (
                <div id={id} style={{margin: 15, textAlign: "center"}}>
                    <label> {v}</label>
                    <div className="mt-3 justify-content-evenly" style={{display: "flex"}}>
                        <div>
                            <input type="radio" id={`cau${id}A`} name={id} value={data[0]}/>
                            <label htmlFor={`cau${id}A`}> {data[0]} </label>
                        </div>
                        <div>
                            <input type="radio" id={`cau${id}B`} name={id} value={data[1]}/>
                            <label htmlFor={`cau${id}B`}> {data[1]} </label>
                        </div>
                        <div>
                            <input type="radio" id={`cau${id}C`} name={id} value={data[2]}/>
                            <label htmlFor={`cau${id}C`}> {data[2]} </label>
                        </div>
                    </div>
                </div>)
        })
    }

    return (
        <Container>
            <Row className="mt-5">
                <Col>
                    <Image
                        src="https://techvccloud.mediacdn.vn/2020/6/18/content-marketing-15924540021511482648370-crop-1592454005885767299662.jpg">
                    </Image>
                </Col>
                <Col>
                    <Col>
                        {
                            isNext ?
                                <div>
                                    {Form2()}
                                    <Button onClick={()=>redirect()} style={{width: '100%'}}>Hoàn tất</Button>
                                </div>
                                :
                                <div>
                                    {Form1()}
                                    <Button className='mt-5' style={{width: '100%'}} onClick={() => NextProcess()}>
                                        Tiếp theo
                                    </Button>
                                </div>
                        }
                    </Col>
                    <ProgressBar className="mt-2" style={{height: "5px", borderRadius: "10%"}} striped variant="success"
                                 now={process}/>
                </Col>
            </Row>
        </Container>
    )
}

export default FormRecommend;