import React, { useState } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Image,
  Navbar,
  Row,
} from "react-bootstrap";
import "./../Training.css";
import LQ from "./../../ExampleData/ListQuestion";
import { useHistory, useLocation } from "react-router-dom";

function TestingPage() {
  const listQuestion = LQ;
  const [choose, setChoose] = useState(false);
  const [questions, setQuestions] = useState(listQuestion);

  // pick answer
  function Choose(e) {
    const tmp = [...questions];
    if (e.target.name) {
      const index = e.target.name;
      tmp[index].checked = true;
    }
    setQuestions(tmp);
  }

  function RenderQuestion() {
    return questions.map((v, id) => {
      return (
        <div key={id} className="mt-3">
          <label style={{ fontWeight: "bold" }}>
            Câu {v.id} : {v.question}
          </label>
          <div>
            <form id={`formCau${id}`} onClick={Choose}>
              <input type="radio" id={`cau${id}A`} name={id} value={`${v.A}`} />
              <label htmlFor={`cau${id}A`}>{v.A} </label>
              <br />
              <input type="radio" id={`cau${id}B`} name={id} value={`${v.B}`} />
              <label htmlFor={`cau${id}B`}> {v.B}</label>
              <br />
              <input type="radio" id={`cau${id}C`} name={id} value={`${v.C}`} />
              <label htmlFor={`cau${id}C`}>{v.C}</label>
              <br />
              <input type="radio" id={`cau${id}D`} name={id} value={`${v.D}`} />
              <label htmlFor={`cau${id}D`}>{v.D}</label>
            </form>
          </div>
        </div>
      );
    });
  }

  function QuestionChecked() {
    return questions.map((v, id) => {
      return (
        <div style={{ display: "inline", margin: 2 }}>
          <Image
            style={{
              border: 1,
              borderRadius: "100%",
              borderStyle: "solid",
              borderColor: "black",
              background: `${v.checked ? "gray" : "white"}`,
              width: 30,
              height: 30,
              margin: "inherit",
            }}
          />
          <span className={`${id + 1 >= 10 ? "txtInside1x" : "txtInside"}`}>
            {id + 1}
          </span>
        </div>
      );
    });
  }

  // redirect
  let history = useHistory();
  let isTestingPage = useLocation().pathname === "/testing";

  function redirect() {
    history.push("/test");
  }

  //count down
  const countDownDate = new Date().getTime() + 3600000; //1 hour
  const CountDown = setInterval(function () {
    // Get today's date and time
    const now = new Date().getTime();

    // Find the distance between now and the count down date
    let distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    if (isTestingPage)
      document.getElementById("count").innerHTML =
        hours + "h " + minutes + "m " + seconds + "s ";

    // If the count down is finished, write some text
    if (distance < 0 && isTestingPage) {
      clearInterval(CountDown);
      document.getElementById("count").innerHTML = "EXPIRED";
    }
  }, 1000);

  return (
    <Container>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-end",
          alignItems: "center",
        }}
      >
        <Navbar.Brand>
          <img src="https://img.icons8.com/emoji/50/000000/alarm-clock-emoji.png" />
        </Navbar.Brand>
        <label id="count">{CountDown}</label>
        <Button style={{ marginLeft: 10 }} onClick={() => redirect()}>
          Nộp bài
        </Button>
      </div>
      <Row>
        <Col>{RenderQuestion()}</Col>
        <Col md={2}>
          <div
            style={{
              border: "solid",
              borderColor: "gray",
              borderWidth: 1,
              padding: 15,
              position: "fixed",
            }}
          >
            <Card.Title>Tổng số câu hỏi là {questions.length}</Card.Title>
            <hr />
            <div>{QuestionChecked()}</div>
            <hr />
            <Card.Subtitle>
              <div>
                <Image
                  style={{
                    border: 1,
                    borderRadius: "100%",
                    borderStyle: "solid",
                    borderColor: "black",
                    background: "gray",
                    width: 30,
                    height: 30,
                    margin: 3,
                  }}
                />
                <span>Câu hỏi đã làm</span>
              </div>
              <div>
                <Image
                  style={{
                    border: 1,
                    borderRadius: "100%",
                    borderColor: "black",
                    borderStyle: "solid",
                    background: "white",
                    width: 30,
                    height: 30,
                    margin: 3,
                  }}
                />
                <span>Câu hỏi chưa làm</span>
              </div>
            </Card.Subtitle>
          </div>
        </Col>
      </Row>
      <div style={{ textAlign: "center" }}>
        <p style={{ fontWeight: "bold", fontSize: 32 }} className="mt5">
          Kết thúc
        </p>
      </div>
      <div style={{ textAlign: "center" }}>
        <hr />
        <Button style={{ marginLeft: 10 }} onClick={() => redirect()}>
          Nộp bài
        </Button>
      </div>
    </Container>
  );
}

export default TestingPage;
