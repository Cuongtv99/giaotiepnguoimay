import React from "react";
import {Button, Card, Container, Row} from "react-bootstrap";
import { useHistory, useLocation } from "react-router-dom";


const SignIn = () => {
    let history = useHistory();
    function redirect() {
        history.push("/form-recommend");
    }

    return (
        <Container>
            <Row className='justify-content-center mt-5'>
                <Card style={{width: "18rem"}}>
                    <form>
                        <h3 className='mt-3' style={{textAlign:"center"}}>LOG IN</h3>
                        <div className="form-group">
                            <label>Email</label>
                            <input type="email" className="form-control" placeholder="Enter email"/>
                        </div>

                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" className="form-control" placeholder="Enter password"/>
                        </div>

                        <div className="form-group">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                                <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                            </div>
                        </div>

                        <div style={{display: "flex", justifyContent: "space-between"}} className='mt-3'>
                            <Button onClick={()=>redirect()} type="submit" className="btn btn-lg btn-block">Sign in</Button>
                            <Button variant='flat' className="btn btn-lg btn-block">Sign Up</Button>

                        </div>
                        <p className="forgot-password text-right mt-2">
                            Forgot <a href="#">password?</a>
                        </p>
                    </form>
                </Card>
            </Row>
        </Container>
    );
};

export default SignIn;
