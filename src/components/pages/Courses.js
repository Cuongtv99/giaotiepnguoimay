import React from "react";
import CoursesPage from "../CoursesPage";
import { Chat } from "react-chat-popup";
import Footer from "../Footer";

function Courses() {
  return (
    <>
      <div className="courses"></div>
      <CoursesPage />
      <Chat />
      <Footer />
    </>
  );
}

export default Courses;
